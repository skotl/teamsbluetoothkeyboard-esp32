More details to follow, but....


This is an app for an ESP32 that will emulate a bluetooth keyboard and allow a remote device to send commands to Microsoft Teams

Commands are:
    Hands up
    (de)Mute microphone
    Hide/show video
    Hangup (with a prolonged button press)
    
