#include <Arduino.h>
#include "../lib/esp32-ble-keyboard/src/BleKeyboard.h"

// Comment out if we don't want to see excessive serial trace
#define DEBUG 1

// Important - uses a hacked version of BleKeyboard from https://github.com/T-vK/ESP32-BLE-Keyboard/issues/128#issuecomment-968280706
#define USE_NIMBLE

RTC_DATA_ATTR int bootCount = 0;	// TODO - remove so we don't leave RTC slow memory powered up

#define BATTERY_VOLTAGE_PIN A13
#define ONBOARD_LED  2

// Button definitions
#define BUTTON_NONE     "NO_ACTION"
#define BUTTON_HANGUP   "BUTTON_HANGUP"
#define BUTTON_HANDSUP  "BUTTON_HANDSUP"
#define BUTTON_MUTE     "BUTTON_MUTE"
#define BUTTON_VIDEO    "BUTTON_VIDEO"

/*
 * We can be in a state where we have not subscribed to a local workstation. In which case, we're going to try
 * and connect in Setup() for a relatively short amount of time and then enter a wait-to-connect state.
 * In that state we will listen for a connection for a period of time and the give up and send the system
 * to sleep if we have not connected.
 */
#define uS_TO_S_FACTOR 1000000      /* Conversion factor for micro seconds to seconds */
#define WAIT_TO_CONNECT         30  /* Expecting to connect for this period (secs) */
#define UNSUBSCRIBED_WAIT_FOR   600 /* Number of seconds we'll wait for a connection, if unsubscribed */
#define UNSUBSCRIBED_SLEEP     1800 /* If we don't subscribe in the interval then hibernate for this period (seconds) */

bool wasNotConnectedAtStartup = false;

typedef struct ButtonState {
    const int pin;
    const char *action;
    const bool needsLongPress;
    int lastRawState;
    int currentState;
    unsigned long debounceTimer;
    unsigned long lastStateChange;
    bool wasLongPress;
};

ButtonState _buttonStates[] = {
        {.pin = 12, .action = BUTTON_HANGUP, .needsLongPress = true, .lastRawState = LOW, .currentState = LOW, .debounceTimer = millis(), .lastStateChange = millis(), .wasLongPress = false },
        {.pin = 15, .action = BUTTON_HANDSUP, .needsLongPress = false, .lastRawState = LOW, .currentState = LOW, .debounceTimer = millis(), .lastStateChange = millis(), .wasLongPress = false }
};

#define NUM_BUTTONS (sizeof(_buttonStates)/sizeof(_buttonStates[0]))
#define DEBOUNCE_DELAY 50
#define LONG_PRESS_MS 500
#define INACTIVITY_TIMER_MS 10000

struct BatteryInfo {
    int literalLevel;
    int reportedLevel;
    float voltage;
    bool charging;
    bool onBattery;
    bool underVoltage;
} _batteryInfo;

void sleep_for_timer(int i);

unsigned long sleepTimer = millis();
unsigned long loopMsgTimer = millis();
uint64_t wakeup_pin = 0;

char batteryTemp[256];  // TODO - REMOVE!


// TODO - Move to setup as we'll recreate this each boot
// TODO - Set battery level
BleKeyboard bleKeyboard("XTEL handXup", "Xtel", 50);

/*
 * Set the button states to defaults and configure
 * input modes for the pins
 */
void reset_button_states()
{
    for(int b=0; b<NUM_BUTTONS;b++)
    {
        _buttonStates[b].lastRawState = LOW;
        _buttonStates[b].currentState = LOW;
        _buttonStates[b].debounceTimer = millis();
        _buttonStates[b].lastStateChange = millis();
        _buttonStates[b].wasLongPress = false;

        pinMode(_buttonStates[b].pin, INPUT);
    }
}

/*
 * Check whether a button really changed state, taking into account debounce logic.
 * Return true if it has, and sets the new state in buttonStates[]
 */
bool button_changed_state(int b)
{
    bool retval = false;

    int reading = digitalRead(_buttonStates[b].pin);
    if (reading != _buttonStates[b].lastRawState)
        _buttonStates[b].debounceTimer = millis();

    if ((millis() - _buttonStates[b].debounceTimer) > DEBOUNCE_DELAY)
    {
        if (reading != _buttonStates[b].currentState)
        {
            _buttonStates[b].currentState = reading;
            _buttonStates[b].wasLongPress = ((millis() - _buttonStates[b].lastStateChange) > LONG_PRESS_MS);
            _buttonStates[b].lastStateChange = millis();
            retval = true;
        }
    }

    _buttonStates[b].lastRawState = reading;

    return retval;
}

unsigned long get_button_bitmask()
{
    unsigned long mask = 0;
    for(int b=0; b<NUM_BUTTONS; b++)
    {
        mask += pow(2, _buttonStates[b].pin);
    }

    return mask;
}

/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason(){
    esp_sleep_wakeup_cause_t wakeup_reason;

    wakeup_reason = esp_sleep_get_wakeup_cause();

    ++bootCount;
    Serial.println("Boot number: " + String(bootCount));

    switch(wakeup_reason)
    {
        case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
        case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
        case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
        case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
        case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
        default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
    }
}

// Read the battery status from the LIPO and print it
// See https://github.com/Torxgewinde/Firebeetle-2-ESP32-E/blob/main/Firebeetle_DeepSleep.ino
// and https://github.com/lobeck/adafruit-feather-huzzah-8266-battery-monitor/blob/master/main.ino
void set_battery_status() {
    int rawLevel = analogRead(BATTERY_VOLTAGE_PIN);
    int level = map(rawLevel, 1600, 2100, 0, 100);
    float realVoltage = (((float)rawLevel)/4095) *2 * 3.3 * 1.1;  //https://cuddletech.com/2017/12/battery-monitoring-on-the-adafruit-huzzah32-esp32-with-mongooseos/

    _batteryInfo.literalLevel = level;
    _batteryInfo.reportedLevel = level > 100 ? 100 : level;
    _batteryInfo.voltage = realVoltage;
    _batteryInfo.charging = rawLevel > 2200 ? true : false;
    _batteryInfo.onBattery = !_batteryInfo.charging;
    _batteryInfo.underVoltage = _batteryInfo.voltage <= 3.2;

#ifdef DEBUG
    Serial.println("Battery levels (literal and reported) are: " + String(_batteryInfo.literalLevel) + "%, " + String(_batteryInfo.reportedLevel) + "%");
    Serial.println("Voltage is " + String(_batteryInfo.voltage));
    Serial.println("Charging, on battery, under voltage = " + String(_batteryInfo.charging) + ", " + String(_batteryInfo.onBattery) + ", " + String(_batteryInfo.underVoltage));
#endif

    snprintf(batteryTemp, 255, "[%d %d %f]\n", rawLevel, level, realVoltage);  // TODO DELETE
}

/*
Method to get the GPIO pin that triggered the wakeup
*/
uint64_t get_GPIO_wake_up(){
    uint64_t GPIO_reason = esp_sleep_get_ext1_wakeup_status();
    uint64_t wakeup_pin = (log(GPIO_reason))/log(2);
    if (wakeup_pin == 0x8000000000000000)
        wakeup_pin = 0;

#ifdef DEBUG
    Serial.println("GPIO that triggered the wake up: GPIO " + String(wakeup_pin));
#endif

    return wakeup_pin;
}

void configure_wakeup_pins()
{
    esp_sleep_enable_ext1_wakeup(get_button_bitmask(),ESP_EXT1_WAKEUP_ANY_HIGH);
}

void sleep_until_pin()
{
    Serial.println("Sleeping until pin hit");
    bleKeyboard.end();
    delay(500);
    esp_deep_sleep_start();
}


void sleep_for_timer(int seconds)
{
    Serial.println("Sleeping in seconds: " + String(seconds));
    esp_sleep_enable_timer_wakeup(seconds * uS_TO_S_FACTOR);
}


/*
 * Initiate variables, understand how we were woken from sleep and set
 * the wakeup conditions
 */
void setup(){
    Serial.begin(115200);
    delay(100); //Take some time to open up the Serial Monitor

    print_wakeup_reason();
    set_battery_status();

    bleKeyboard.begin();
    Serial.println("Waiting for client subscription");
    ulong waitSubscription = millis();

    while (!bleKeyboard.isSubscribed() && millis() - waitSubscription < (WAIT_TO_CONNECT * 1000)) {
        Serial.print(".");
        delay(250);
    }

    if (!bleKeyboard.isSubscribed())
    {
        wasNotConnectedAtStartup = true;
        Serial.println("\r\nFailed to connect in setup");
        return;     // Execute Loop() but just look for subscription
    }


    Serial.println("subscribed");

    // If we were woken by a switch press, record this to handle in loop()
    wakeup_pin = get_GPIO_wake_up();

    // Reset button states to default
    reset_button_states();

    // Enable wake on each of the buttons
    configure_wakeup_pins();

    // Reset the sleep timers
    sleepTimer = millis();
    loopMsgTimer = millis();

    bleKeyboard.setBatteryLevel(_batteryInfo.reportedLevel);

    pinMode(ONBOARD_LED,OUTPUT);

    return;
}

/*
 * Check whether we have pressed a button, either because the user has pressed
 * and released a button *now* (taking debouce into account), or the ESP32
 * was woken from sleep and we recorded which button was used to wake it.
 */
void loop(){
    int buttonPressed = 0;
    const char *action = BUTTON_NONE;

#ifdef DEBUG
    if (millis() - loopMsgTimer > 2000)
    {
        Serial.println("Still looping...");
        Serial.println((String)"isConnected = " + (bleKeyboard.isConnected() ? "true" : "false"));  // TODO remove
        Serial.println((String)"isSubscribed = " + (bleKeyboard.isSubscribed() ? "true" : "false"));  // TODO remove

        loopMsgTimer = millis();
    }

#endif

    if (!bleKeyboard.isSubscribed())    // Likely because we have not yet connected to a computer,
    {                                   // ...so stay awake long enough to connect
        Serial.println("In loop, unsubscribed so waiting for connection");
        ulong waitForSubscription = millis();
        while (!bleKeyboard.isSubscribed() && millis()-waitForSubscription < (UNSUBSCRIBED_WAIT_FOR * 1000))
        {
            Serial.print(".");
            delay(250);
        }

        if (bleKeyboard.isSubscribed())
        {
            configure_wakeup_pins();
            sleep_until_pin();
            return; // never reached
        } else
        {
            sleep_for_timer(UNSUBSCRIBED_SLEEP);
            return; // never reached
        }
    }

    if (*batteryTemp != '\0' && bleKeyboard.isConnected())  // TODO Remove
    {
        //bleKeyboard.print(batteryTemp);
        int x = 0;
        char c = '?';
        while(c != '\0')
        {
            c = batteryTemp[x];
            bleKeyboard.press(c);
            delay(30);
            bleKeyboard.releaseAll();

            x++;
        }
        *batteryTemp = '\0';
    }


    if (wakeup_pin != 0)
    {
        for(int b=0; b<NUM_BUTTONS; b++)
        {
            if (wakeup_pin == _buttonStates[b].pin)
            {
                buttonPressed = wakeup_pin;
                action = _buttonStates[b].action;
            }
        }

        wakeup_pin = 0;
    }

    for(int b=0; b<NUM_BUTTONS; b++)
    {
        if (button_changed_state(b))
        {
#ifdef DEBUG
            Serial.print("Button changed state, pin=" + String(_buttonStates[b].pin));
            Serial.print(", state is now: ");
            if (_buttonStates[b].currentState == LOW)
                Serial.println("low");
            else
                Serial.println("high");
#endif

            // Key released, so execute on this button
            if (_buttonStates[b].currentState == LOW)
            {
                buttonPressed = _buttonStates[b].pin;
                if (!_buttonStates[b].needsLongPress || _buttonStates[b].wasLongPress)
                    action = _buttonStates[b].action;
            }
        }
    }

    if (buttonPressed != 0)
    {
        Serial.println("There was a button pressed: " + String(buttonPressed) + " " + action);

        sleepTimer = millis();    // Reset countdown to sleep
        buttonPressed = 0;
        digitalWrite(ONBOARD_LED,HIGH);
        delay(10);
        digitalWrite(ONBOARD_LED,LOW);
    }

    if (action != BUTTON_NONE && bleKeyboard.isConnected())
    {
        bleKeyboard.println(action);
        bleKeyboard.setBatteryLevel(69);  // TODO: Remove
    }

    if (millis() - sleepTimer > INACTIVITY_TIMER_MS)
    {
        Serial.println("No activity - sleeping");
        bleKeyboard.end();
        delay(1000);
        esp_deep_sleep_start();
        return;   // Never executed
    }
}
